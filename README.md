# Nix Website
Sitio web de [**Nix**](http://nix.jorgebrunal.co/)


### Requisitos (Despliegue)
- [Apache](http://www.apache.org/) Versión 2.2.x o compatible


## Requisitos (Desarrollo)
- [nodejs](https://nodejs.org/en/) Versión 4.1.2 ó compatible
- [npm](https://www.npmjs.com/) Versión 2.14.4 ó compatible
- [Gulp](http://gulpjs.com/) Versión 3.9.0 (**CLI**) ó superior
- [Bower](http://bower.io/) Versión  1.7.2 ó superior

Para el desarrollo y construcción se utiliza [**Gulp**](http://gulpjs.com/) y se manejan las dependencias con [**Bower.**](http://bower.io/)
Todo lo anterior se ejecuta sobre **[NodeJS](http://nodejs.org/)**.

Para construir el proyecto con **Gulp** y administrar dependencias con **Bower**. Se necesita lo siguiente:

A fin de utilizar **Gulp** necesitará tener instalado **Node.js**
También asegurese que ha instalado [**NPM**](https://www.npmjs.org/).

Usted puede comprobar si estos están instalados correctamente mediante la apertura de una Terminal (Linux o Mac) o Sá­mbolo del sistema (Windows)
e introduciendo el comando: **node --version** y **npm --version** Estos comandos deben ser algo similar a "**v4.5.0**" y "**2.15.9**", respectivamente, como número de versiones mí­nima.


## Instalacion de este repositorio
Clonar este repositorio y alojarlo en una carpeta conveniente.

    git clone git@bitbucket.org:diniremix/nix.git


Una vez que ha instalado estos programas y clonado el repositorio, usted necesita obtener **Gulp** y **Bower**. Usted puede lograr esto mediante la ejecución de los siguientes comandos:

```sh
sudo npm install -g bower gulp-cli
```


### Instalar dependencias del proyecto
```sh
sudo npm install
```

```sh
bower install
```

### Instalar herramientas adicionales
```sh
$ sudo npm install -g html2jade js2coffee
```

## Construir usando Gulp

```sh
gulp build
```

>*Una vez terminada la construcción con el comando anterior, se creará una carpeta llamada **dist**, en la carpeta raá­z del proyecto, donde se encuentra el sitio preparado para producción.* 


>*Tenga en cuenta que los comandos anteriores se deben ejecutar desde la carpeta raíz del proyecto.*


### Acerca de **gulpfile.js**
El archivo **gulpfile.js** ubicado en  la carpeta raíz del proyecto, contiene las tareas para construir y gestionar el proyecto, así­ como un servidor web local para la realización de pruebas.


* Ejecutar el servidor web ***como se comentó más arriba*** y dirigirse a **http://localhost:9876/**
* Editor recomendado [SublimeText 3](http://www.sublimetext.com/3)


### Contacto ###
[Jorge Brunal](http://jorgebrunal.co)

email: *diniremix [at] gmail [dot] com*
