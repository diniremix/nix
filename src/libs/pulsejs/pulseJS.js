/*! pulseJS 0.5.2 */
var pulseJS = (function() {
    var VERSION = '0.5.2';
    var URL_BASE,
        AUTH_TOKEN_NAME,
        AUTH_KEY_TOKEN;

    _validateData = function(object) {
        var stringConstructor = "pulse".constructor;
        var arrayConstructor = [].constructor;
        var objectConstructor = {}.constructor;

        if (object === null) {
            return "null";
        } else if (object === undefined) {
            return "undefined";
        } else if (object.constructor === stringConstructor) {
            return "String";
        } else if (object.constructor === arrayConstructor) {
            return "Array";
        } else if (object.constructor === objectConstructor) {
            return "Object";
        } else {
            return "unknow";
        }
    }


    getFormsElements = function(form) {
        var regForm = $('#' + form).serializeArray();
        var regFormObject = {};
        var cont = 0;
        $.each(regForm, function(i, v) {
            regFormObject[v.name] = v.value;
        });
        return regFormObject;
    };

    validateForms = function(form) {
        var st = true;
        $.each(form, function(i, v) {
            if (!v || 0 === v.length) {
                st = false;
            }
        });
        return st;
    };

    redirect = function(page) {
        document.location = page;
    };

    asJson = function(obj) {
        var jsonForm = JSON.stringify(obj, null, 2);
        return jsonForm;
    };

    isJson = function(cad) {
        try {
            var json = JSON.parse(cad);
        } catch (e) {
            return false;
        }
        return true;
    };

    ajax = function(url, method, data, dataType, callback) {
        var _myurl,
            _mydata,
            _result;

        _myurl = URL_BASE + url;
        _result = _validateData(data);
        switch (_result) {
            case "null":
            case "Object":
                _mydata = data;
                break;
            case "String":
                if (!data || 0 === data.length) {
                    console.error("Data (String) must not be empty");
                    return false;
                }
                //use encodeURI
                _myurl += '?' + data;
                break;
            case "unknow":
            default:
                console.error("Data must be Object or String");
                return false;
                break;
        }

        $.ajax({
            url: _myurl,
            type: method ? method : 'GET',
            data: _mydata ? _mydata : {},
            dataType: dataType ? dataType : 'json',
            /*contentType: "application/json;charset=utf-8",
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,*/
            beforeSend: function(request) {
                if (AUTH_KEY_TOKEN) {
                    request.setRequestHeader(AUTH_TOKEN_NAME, AUTH_KEY_TOKEN);
                }
            },
            success: function(data) {
                if (data.status) {
                    if (data.status == 200) {
                        (typeof(callback) === 'function') ? callback({ status: data.status, data: data.data }): null;
                    } else {
                        (typeof(callback) === 'function') ? callback({ status: data.status, message: data.message }): null;
                    }
                } else {
                    (typeof(callback) === 'function') ? callback(data): null;
                }
            },
            error: function(data, status) {
                var error = {
                    status: data.status ? data.status : '',
                    statusText: data.statusText ? data.statusText : '',
                    responseText: data.responseText ? data.responseText : ''
                };
                (typeof(callback) === 'function') ? callback({status:status, error: error}): null;
            }
        });
    };

    config = function(configuration) {
        URL_BASE = configuration.url_base ? configuration.url_base : "api/v1/";
        AUTH_TOKEN_NAME = configuration.token_name ? configuration.token_name : "public_key_token";
        AUTH_KEY_TOKEN = configuration.public_key_token ? configuration.public_key_token : "";

        console.info("Welcome to pulseJS Version:", VERSION);
        console.info("pulseJS configuration", URL_BASE, AUTH_TOKEN_NAME, AUTH_KEY_TOKEN);
    };

    return {
        getFormsElements: getFormsElements,
        validateForms: validateForms,
        redirect: redirect,
        asJson: asJson,
        isJson: isJson,
        callAjax: ajax,
        config: config
    };
})();
