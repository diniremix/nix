$(window).load ->
  $container = $('.blog-grid')
  gutter = 30
  min_width = 345
  $container.imagesLoaded ->
    $container.masonry
      itemSelector: '.post'
      gutterWidth: gutter
      isAnimated: true
      columnWidth: (containerWidth) ->
        box_width = (containerWidth - gutter) / 2 | 0
        if box_width < min_width
          box_width = (containerWidth - gutter) / 2 | 0
        if box_width < min_width
          box_width = containerWidth
        $('.post').width box_width
        box_width
    $container.css('visibility', 'visible').parent().removeClass 'loading'
    return
  return
jQuery(document).ready ($) ->
  $('.video').fitVids()
  return
jQuery(document).ready ($) ->
  $('a.button, .forms fieldset .btn-submit, #commentform input#submit').css 'opacity', '1.0'
  $('a.button, .forms fieldset .btn-submit, #commentform input#submit').hover (->
    $(this).stop().animate { opacity: 0.85 }, 'fast'
    return
  ), ->
    $(this).stop().animate { opacity: 1.0 }, 'fast'
    return
  return
jQuery(document).ready ($) ->
  $('.quick-flickr-item').addClass 'frame'
  $('.frame a').prepend '<span class="more"></span>'
  return
jQuery(document).ready ($) ->
  $('.frame').mouseenter((e) ->
    $(this).children('a').children('span').fadeIn 300
    return
  ).mouseleave (e) ->
    $(this).children('a').children('span').fadeOut 200
    return
  return
getTwitters 'twitter',
  id: 'elemisdesign'
  count: 2
  enableLinks: true
  ignoreReplies: false
  template: '<span class="twitterPrefix"><span class="twitterStatus">%text%</span><br /><em class="twitterTime"><a href="http://twitter.com/%user_screen_name%/statuses/%id%">%time%</a></em>'
  newwindow: true
$(document).ready ($) ->
  $('.flickr-feed').dcFlickr
    limit: 9
    q:
      id: '51789731@N07'
      lang: 'en-us'
      format: 'json'
      jsoncallback: '?'
    onLoad: ->
      $('.frame a').prepend '<span class="more"></span>'
      $('.frame').mouseenter((e) ->
        $(this).children('a').children('span').fadeIn 300
        return
      ).mouseleave (e) ->
        $(this).children('a').children('span').fadeOut 200
        return
      return
  return
ddsmoothmenu.init
  mainmenuid: 'menu'
  orientation: 'h'
  classname: 'menu'
  contentsource: 'markup'
