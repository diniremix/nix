$(document).ready ->
  validateEmail = (email) ->
    re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    re.test email

  #submit message
  $('#submitEmail').click (e) ->
    console.log 'sending...'
    frmsend = pulseJS.getFormsElements('frmsend')
    emailValid = validateEmail frmsend.email
    isValid = pulseJS.validateForms(frmsend)
    if isValid and emailValid
      $('#frmsend')[0].reset()
      pulseJS.callAjax 'emailme', 'POST', frmsend, 'json', (data, status) ->
        if data.errorCode == 0
          console.log "Gracias por tus comentarios!"
        else if data.errorCode > 0
          console.log "Ops!, Ocurrio un error al enviar tu mensaje"
        else
          console.log "Ops!, Ocurrio un error, Estas conectado a internet?"
        return
    else
      alert 'Escribe tu nombre y un mensaje...'
    return
  
  # pulseJS config
  pulseJS.config
    url_base: 'api/v1/'
    token_name: 'public_token'
    public_key_token: ''
    data_request: ''
  return
