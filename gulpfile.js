/*
 *
 * Gulpfile Configuration for nix
 * Author: @Diniremix
 * Version: 0.0.1
 **/
var args = require('yargs').argv,
    path = require('path'),
    through = require('through2'),
    gulp = require('gulp'),
    connect = require('gulp-connect'),
    $ = require('gulp-load-plugins')(),
    gulpsync = $.sync(gulp),
    PluginError = $.util.PluginError,
    del = require('del'),
    server_port = 9876;

// production mode (see build task)
var isProduction = false;
// styles sourcemaps
var useSourceMaps = false;

// Switch to sass mode. 
// Example:
//    gulp --usesass
var useSass = args.usesass;

// Angular template cache
// Example:
//    gulp --usecache
var useCache = args.usecache;

// ignore everything that begins with underscore
var hidden_files = '**/_*.*';
var ignored_files = '!' + hidden_files;

// MAIN PATHS
var paths = {
    app: 'dist/',
    views: 'src/views/',
    styles: 'src/styles/',
    coffee: 'src/coffee/',
    img: 'src/images/'
}

// if sass -> switch to sass folder
if (useSass) {
    log('Using SASS stylesheets...');
    paths.styles = 'src/styles/sass/';
}

// VENDOR CONFIG
var vendor = {
    // vendor scripts required to start the app
    base: {
        source: require('./vendor.json'),
        dest: 'dist/js',
        name: 'vendor.js'
    }
};


// SOURCES CONFIG 
var source = {
    scripts: [
        // coffee modules
        paths.coffee + '*.coffee'
    ],
    styles: {
        app: [paths.styles + '*.*'],
        themes: [paths.styles + 'themes/*'],
        watch: [paths.styles + '**/*', '!' + paths.styles + 'themes/*']
    },
    templates: {
        index: ['src/index.jade'],
        views: [paths.views + '*.jade']
    }

};

// BUILD TARGET CONFIG 
var build = {
    scripts: paths.app + 'js',
    coffee: paths.app + 'coffee',
    styles: paths.app + 'css',
    images: paths.app + 'images',
    templates: {
        index: 'dist/',
        views: paths.app,
        cache: paths.app + 'js/' + 'templates.js',
    }
};

// PLUGINS OPTIONS

var prettifyOpts = {
    indent_char: ' ',
    indent_size: 3,
    unformatted: ['a', 'sub', 'sup', 'b', 'i', 'u', 'pre', 'code']
};

var vendorUglifyOpts = {
    mangle: {
        except: ['$super'] // rickshaw requires this
    }
};

var compassOpts = {
    project: path.join(__dirname, './'),
    css: 'dist/css',
    sass: 'dist/sass/',
    image: 'dist/images'
};

var compassOptsThemes = {
    project: path.join(__dirname, './'),
    css: 'dist/css',
    sass: 'dist/sass/themes/', // themes in a subfolders
    image: 'dist/images'
};

var tplCacheOptions = {
    root: 'dist',
    filename: 'templates.js',
    //standalone: true,
    module: 'app.core',
    base: function(file) {
        return file.path.split('jade')[1];
    }
};

var injectOptions = {
    name: 'templates',
    transform: function(filepath) {
        return 'script(src=\'' +
            filepath.substr(filepath.indexOf('dist')) +
            '\')';
    }
}

//---------------
// TASKS
//---------------


// JS APP
gulp.task('scripts:app', function() {
    log('Building scripts..');
    var coffeeFilter = $.filter('**/*.coffee');
    // Minify and copyall JavaScript (except vendor scripts)
    return gulp.src(source.scripts)
        .pipe(coffeeFilter)
        .pipe($.coffee())
        .on('error', handleError)
        .pipe(coffeeFilter.restore())
        .pipe($.jsvalidate())
        .on('error', handleError)
        .pipe($.if(useSourceMaps, $.sourcemaps.init()))
        .pipe($.concat('app.js'))
        .on('error', handleError)
        .pipe($.if(isProduction, $.uglify({ preserveComments: 'some' })))
        .on('error', handleError)
        .pipe($.if(useSourceMaps, $.sourcemaps.write()))
        .pipe(gulp.dest(build.scripts));
});

// VENDOR BUILD
gulp.task('vendor', gulpsync.sync(['vendor:base']));

// VENDOR ASSETS
gulp.task('assets', ['scripts:app', 'styles:app', 'images', 'templates']);

// Build the base script to start the application from vendor assets
gulp.task('vendor:base', function() {
    log('Copying base vendor assets..');
    return gulp.src(vendor.base.source)
        .pipe($.expectFile(vendor.base.source))
        .on('error', handleError)
        .pipe($.if(isProduction, $.uglify()))
        .pipe($.concat(vendor.base.name))
        .on('error', handleError)
        .pipe(gulp.dest(vendor.base.dest));
});


// copy file from images folder into the app images folder
gulp.task('images', function() {
    log('Copying images...');
    return gulp.src(paths.img + '**/*.*')
        .pipe($.expectFile(paths.img + '**/*.*'))
        .pipe(gulp.dest(paths.app + 'images'));
});


// APP LESS
gulp.task('styles:app', function() {
    log('Building application styles..');
    return gulp.src(source.styles.app)
        .pipe($.if(useSourceMaps, $.sourcemaps.init()))
        .on('error', handleError)
        .pipe($.if(isProduction, $.minifyCss()))
        .on('error', handleError)
        .pipe(gulp.dest(build.styles));
});

// JADE ASSETS
gulp.task('templates', ['templates:index', 'templates:views']);

// JADE
gulp.task('templates:index', function() {
    log('Building index..');
    return gulp.src(source.templates.index)
        .pipe($.changed(build.templates.index, { extension: '.html' }))
        .pipe($.jade())
        .on('error', handleError)
        .pipe($.htmlPrettify(prettifyOpts))
        .on('error', handleError)
        .pipe(gulp.dest(build.templates.index));
});

// JADE
gulp.task('templates:views', function() {
    log('Building views.. ' + (useCache ? 'using cache' : ''));

    if (useCache) {
        return gulp.src(source.templates.views)
            .pipe($.jade())
            .on('error', handleError)
            .pipe($.if(isProduction, $.uglify({ preserveComments: 'some' })))
            .on('error', handleError)
            .pipe(gulp.dest(build.templates.index));;
    } else {
        return gulp.src(source.templates.views)
            .pipe($.if(!isProduction, $.changed(build.templates.views, { extension: '.html' })))
            .pipe($.jade())
            .on('error', handleError)
            .pipe($.htmlPrettify(prettifyOpts))
            .on('error', handleError)
            .pipe(gulp.dest(build.templates.index));
    }
});

// lint javascript
gulp.task('jslint', function() {
    var jsFilter = $.filter('**/*.js');
    return gulp
        .src(source.scripts)
        .pipe(jsFilter)
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish', { verbose: true }))
        .pipe($.jshint.reporter('fail'))
        .on('error', handleError)
        .pipe(jsFilter.restore());
});

// lint coffescript
gulp.task('coffeelint', function() {
    var coffeeFilter = $.filter('**/*.coffee');
    return gulp
        .src(source.scripts)
        .pipe(coffeeFilter)
        .pipe($.coffeelint())
        //.pipe($.coffeelint.reporter('coffeelint-stylish'))
        .pipe($.coffeelint.reporter())
        .on('error', handleError)
        .pipe(coffeeFilter.restore())
});

// Remove all files from the build paths
gulp.task('clean', function(done) {
    var delconfig = [].concat(
        build.styles,
        build.images,
        build.scripts,
        build.templates.index + '*.html',
        build.templates.views + 'views'
    );

    log('Cleaning: ' + $.util.colors.blue(delconfig));
    // force: clean files outside current directory
    del(delconfig, { force: true }, done);
});

//---------------
// MAIN TASKS
//---------------

gulp.task('prod', function() {
    log('Starting production build...');
    isProduction = true;
});

// build for production (minify)
gulp.task('build', gulpsync.sync([
    'clean',
    'prod',
    'vendor',
    'assets',
    'images',
]));


// build with sourcemaps (no minify)
gulp.task('sourcemaps', ['usesources', 'default']);
gulp.task('usesources', function() { useSourceMaps = true; });

// default (no minify)
gulp.task('default', gulpsync.sync([
    'vendor',
    'assets',
    'watch'
]), function() {

    log('************');
    log('* All Done * You can start editing your code, LiveReload will update your browser after any change..');
    log('************');

});

//---------------
// WATCH
//---------------
//
gulp.task('webserver', ['watch'], function() {
    connect.server({
        livereload: true,
        root: ['dist/'],
        port: server_port
    });
});

//
// Rerun the task when a file changes
gulp.task('watch', function() {
    log('Starting watch and LiveReload..');

    $.livereload.listen();

    gulp.watch(source.scripts, ['scripts:app']);
    gulp.watch(source.styles.watch, ['styles:app']);
    
    gulp.watch(source.templates.views, ['templates:views']);
    gulp.watch(source.templates.index, ['templates:index']);

    // a delay before triggering browser reload to ensure everything is compiled
    var livereloadDelay = 1500;
    // list of source file to watch for live reload
    var watchSource = [].concat(
        source.scripts,
        source.styles.watch,
        source.templates.views,
        source.templates.index
    );

    gulp
        .watch(watchSource)
        .on('change', function(event) {
            setTimeout(function() {
                $.livereload.changed(event.path);
            }, livereloadDelay);
        });
});


// Error handler
function handleError(err) {
    log(err.toString());
    this.emit('end');
}

// Mini gulp plugin to flip css (rtl)
function flipcss(opt) {

    if (!opt) opt = {};

    // creating a stream through which each file will pass
    var stream = through.obj(function(file, enc, cb) {
        if (file.isNull()) return cb(null, file);

        if (file.isStream()) {
            // Todo: isStream!
        }

        var flippedCss = flip(String(file.contents), opt);
        file.contents = new Buffer(flippedCss);
        cb(null, file);
    });

    // returning the file stream
    return stream;
}

// log to console using 
function log(msg) {
    $.util.log($.util.colors.blue(msg));
}
